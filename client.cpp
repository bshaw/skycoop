
#include "client.h"
#include "memory.h"

int GetPlayerCoordinates(HANDLE handle, int baseaddr, S_CLIENT* user) {

	user->x = ReadPointerF(handle, baseaddr, 0x2f0, 0x6c, 0x8, 0x60, 0xe0);
	user->y = ReadPointerF(handle, baseaddr, 0x2f0, 0x6c, 0x8, 0x60, 0xe8);
	user->z = ReadPointerF(handle, baseaddr, 0x2f0, 0x6c, 0x8, 0x60, 0xe4);
	return 1;

}

int SetSpawnedPlayerCoordinates(HANDLE handle, int baseaddr, float x, float y, float z) {


	WritePointerF(handle, x, baseaddr, 0x36c, 0xcc, 0x60, 0x3c, 0x130);
	WritePointerF(handle, y, baseaddr, 0x36c, 0xcc, 0x60, 0x3c, 0x138);
	WritePointerF(handle, z, baseaddr, 0x36c, 0xcc, 0x60, 0x3c, 0x134);


}
