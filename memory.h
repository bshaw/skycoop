#ifndef MEMORY_H
#define MEMORY_H
#include <iostream>

float ReadPointerF(HANDLE handle, int address, int off1, int off2, int off3, int off4, int off5);
int WritePointerF(HANDLE handle, float value, int address, int off1, int off2, int off3, int off4, int off5);
int GetProcessID(std::string processName);
int GetModuleBase(std::string module, int pid);

#endif // MEMORY_H
