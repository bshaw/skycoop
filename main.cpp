#include <iostream>
#include "conio.h"
#include "stdio.h"

#include "net.h"
#include "util.h"
#include "stream.h"

#include "client.h"
#include "memory.h"


int main(int argc, char* argv[]) {

	int client = 0;
	int isRunning = 1;
	int mode = 0;

	std::string tmpAddress;

	SimpleNet *net = new SimpleNet();
	S_CLIENT players[1];
	players[1] = {0};

	int in = getch();
	if (in == 'c') {

		mode = 2;

		net->StartClient("69.54.4.139", "2220");

	} else {

		mode = 1;
		net->StartServer(2, "2220");

	}

	std::cout << "Launch Skyrim, load into the game\nOpen console and spawn an npc\nOnce done, press any key to continue...\n";
	pause();

	int pid = GetProcessID("TESV.exe");
	int baseplayer_addr = GetModuleBase("TESV.exe", pid) + P_PLAYER_BASE;
	int spawnedplayer_addr = GetModuleBase("TESV.exe", pid) + P_SPAWNED_BASE;
	HANDLE handle = OpenProcess(PROCESS_ALL_ACCESS, 1, pid);

	while (mode == 1) { // server mode
		Sleep(33);
		client = net->Listen();
		if (client > 0) {

			switch(net->NetworkEvent()) {

				case SN_RECEIVE:
					players[1] = net->Receive(client);
					std::cout << players[1].x << "\n";
					std::cout << players[1].y << "\n";
					std::cout << players[1].z << "\n";

					break;
				case SN_CONNECT:

					if (players[1].sock != client) {

						players[1].sock = client;

					}
					std::cout << "Client attempted to connect\n";
					net->msgout = (SNStream*) malloc(sizeof(SNStream));
					net->msgout->type = SN_MSGCONNECTED;
					net->Send(client, net->msgout);
					free(net->msgout);
					break;
				case SN_DISCONNECT:
					std::cout << "Disconnected\n";
					break;

			}

		}

		// send client 1 the servers coordinates
		// dear anyone:
		// I am sorry, I had to do this. The voices, they told me to.
		// Usually, I just ignore them. But today they made sure they would not go unheard...

		if (players[1].sock != 0) {
			GetPlayerCoordinates(handle, baseplayer_addr, &players[0]);

			net->msgout = (SNStream*) malloc(sizeof(SNStream));
			net->msgout->type = SN_MSGUPDATE;
			memcpy(net->msgout->message, ((char*)&players[0].x), sizeof(float));
			memcpy(net->msgout->message+4, ((char*)&players[0].y), sizeof(float));
			memcpy(net->msgout->message+8, ((char*)&players[0].z), sizeof(float));

			net->Send(players[1].sock, net->msgout);
			free(net->msgout);

		}

	}

	while (mode == 2) { // client mode

		Sleep(33);

		client = net->Listen();
		if (client > 0) {

			switch(net->NetworkEvent()) {

				case SN_RECEIVE:
					std::cout << "received crap from the server\n";
					players[1] = net->Receive(client);
					std::cout << players[1].x << "\n";
					std::cout << players[1].y << "\n";
					std::cout << players[1].z << "\n";
					//std::cout << players[1].sock;
					break;
				case SN_CONNECT:
					std::cout << "Connected\n";
					break;
				case SN_DISCONNECT:
					std::cout << "Disconnected\n";
					mode = 0;
					break;

			}

		}

		if (net->sn_connected != 0) {
			GetPlayerCoordinates(handle, baseplayer_addr, &players[0]);

			net->msgout = (SNStream*) malloc(sizeof(SNStream));
			net->msgout->type = SN_MSGUPDATE;
			memcpy(net->msgout->message, ((char*)&players[0].x), sizeof(float));
			memcpy(net->msgout->message+4, ((char*)&players[0].y), sizeof(float));
			memcpy(net->msgout->message+8, ((char*)&players[0].z), sizeof(float));

			net->Send(net->sn_sockMain, net->msgout);
			free(net->msgout);

		}

	}

	pause();
	return 0;

}
