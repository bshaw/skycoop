
#ifndef CLIENT_H
#define CLIENT_H

#include "windows.h"

struct S_CLIENT {

	int sock;

	float x;
	float y;
	float z;

};

enum {

	P_PLAYER_BASE = 0x00ED3DE4,
	P_SPAWNED_BASE = 0x00EE41EC

};

int GetPlayerCoordinates(HANDLE handle, int baseaddr, S_CLIENT* user);
int SetSpawnedPlayerCoordinates(HANDLE handle, int baseaddr, float x, float y, float z);
/*
	int realAddress = GetModuleBase("TESV.exe", pid) + 0x00ED3DE4;

	HANDLE handle = OpenProcess(PROCESS_ALL_ACCESS, 1, pid);

	int offOne = 0x2f0;
	int offTwo = 0x6c;
	int offThree = 0x8;
	int offFour = 0x60;
	int offFive = 0xe0; // e0 = x? e4 = z?, e8 = y

	one = 0x36c
	two = 0xcc
	three = 0x60
	four = 0x3c
	five = 0x130; // 130 = x? 134 = z? 138 = y

	*/

#endif // CLIENT_H


