#include <iostream>

#include "windows.h"
#include "TlHelp32.h"


float ReadPointerF(HANDLE handle, int address, int off1, int off2, int off3, int off4, int off5) {

	float buffer = 0.0f;
    int tAddr = address;

	ReadProcessMemory(handle, (LPCVOID)tAddr, &tAddr, 4, 0);
	tAddr += off1;

	ReadProcessMemory(handle, (LPCVOID)tAddr, &tAddr, 4, 0);
	tAddr += off2;

	ReadProcessMemory(handle, (LPCVOID)tAddr, &tAddr, 4, 0);
	tAddr += off3;

	ReadProcessMemory(handle, (LPCVOID)tAddr, &tAddr, 4, 0);
	tAddr += off4;

	ReadProcessMemory(handle, (LPCVOID)tAddr, &tAddr, 4, 0);
	tAddr += off5;

	ReadProcessMemory(handle, (void*)tAddr, &buffer, 4, 0);
	return buffer;

}

int WritePointerF(HANDLE handle, float value, int address, int off1, int off2, int off3, int off4, int off5) {

    int tAddr = address;

	ReadProcessMemory(handle, (LPCVOID)tAddr, &tAddr, 4, 0);
	tAddr += off1;

	ReadProcessMemory(handle, (LPCVOID)tAddr, &tAddr, 4, 0);
	tAddr += off2;

	ReadProcessMemory(handle, (LPCVOID)tAddr, &tAddr, 4, 0);
	tAddr += off3;

	ReadProcessMemory(handle, (LPCVOID)tAddr, &tAddr, 4, 0);
	tAddr += off4;

	ReadProcessMemory(handle, (LPCVOID)tAddr, &tAddr, 4, 0);
	tAddr += off5;

	WriteProcessMemory(handle, (LPVOID)tAddr, &value, 4, 0);
	return 1;
}

int GetProcessID(std::string processName) {

	HANDLE snap;
	PROCESSENTRY32 pe;

	snap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	pe.dwSize = sizeof(PROCESSENTRY32);


	Process32First(snap, &pe);

	while (Process32Next(snap, &pe)) {
		//cout << std::dec << GetLastError() << "\n";
		if (!strcmp(pe.szExeFile, processName.c_str())) {

			std::cout << "Found the process: " <<  pe.szExeFile << "\n";
			CloseHandle(snap);
			return /*OpenProcess(PROCESS_ALL_ACCESS, 1, */pe.th32ProcessID;

		}

	}

	CloseHandle(snap);

	return 0;

}

int GetModuleBase(std::string module, int pid) {

	HANDLE snap;
	MODULEENTRY32 me;
	me.dwSize = sizeof(MODULEENTRY32);

	snap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);

	Module32First(snap, &me);
	std::cout << "First Module: ";
	if (!strcmp(me.szModule, module.c_str())) {

		std::cout << "Found a module\n";
		CloseHandle(snap);
		return (int)me.hModule;

	}
	while(Module32Next(snap, &me)) {

		std::cout << "This Module: ";
		std::cout << me.szModule << "\n";

		if (!strcmp(me.szModule, module.c_str())) {

			std::cout << "Found a module\n";
			CloseHandle(snap);
			return (int)me.hModule;

		}

	}

	CloseHandle(snap);
	return 0;

}
